import { makeRecord } from "sdi/source";
import { fromRecord } from "sdi/locale";

export const attribution = () => "cartofixer.be";

export const credits = () =>
  fromRecord(makeRecord("Fond de plan: ", "Achtergrond: "));
